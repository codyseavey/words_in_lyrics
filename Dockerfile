FROM node:latest

ADD app lyrics
ADD certs /etc/certs
WORKDIR /lyrics/
RUN npm install

EXPOSE 8443
EXPOSE 8080
CMD npm start
